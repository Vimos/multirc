import tensorflow as tf
from tensorflow.python.ops.rnn_cell_impl import BasicLSTMCell

from func import dense, dot_attention, CuDnnGRU, NativeGRU, softmax_mask
from layers import total_params, conv
from my.tensorflow.rnn import bidirectional_dynamic_rnn
from my.tensorflow.rnn_cell import SwitchableDropoutWrapper


class Model(object):
    def __init__(self, config, batch, word_mat=None, char_mat=None, trainable=True, opt=True, demo=False, graph=None):
        self.config = config
        self.demo = demo
        self.is_train = not self.demo
        self.graph = graph if graph is not None else tf.Graph()
        with self.graph.as_default():

            self.global_step = tf.get_variable('global_step', shape=[], dtype=tf.int32,
                                               initializer=tf.constant_initializer(0), trainable=False)
            self.dropout = tf.placeholder_with_default(0.0, (), name="dropout")
            if self.demo:
                self.c = tf.placeholder(tf.int32,
                                        [None, config.test_paragraph_limit, config.test_sentence_limit],
                                        "context")
                self.q = tf.placeholder(tf.int32,
                                        [None, config.test_question_limit],
                                        "question")
                self.a = tf.placeholder(tf.int32,
                                        [None, config.test_candidate_limit, config.test_answer_limit],
                                        "answer")
                self.ch = tf.placeholder(tf.int32,
                                         [None, config.test_paragraph_limit, config.test_sentence_limit,
                                          config.char_limit],
                                         "context_char")
                self.qh = tf.placeholder(tf.int32,
                                         [None, config.test_question_limit, config.char_limit],
                                         "question_char")
                self.ah = tf.placeholder(tf.int32,
                                         [None, config.test_candidate_limit, config.test_sentence_limit,
                                          config.char_limit],
                                         "answer_char")
                self.y = tf.placeholder(tf.int32, [None, config.test_candidate_limit], "answer_index2")
                self.su = tf.placeholder(tf.int32, [None, config.test_paragraph_limit], "sentences_used")
            else:
                self.c, self.q, self.a, self.ch, self.qh, self.ah, self.y, self.su, self.qa_id = batch.get_next()

            # self.word_unk = tf.get_variable("word_unk", shape = [config.glove_dim], initializer=initializer())
            self.word_mat = tf.get_variable("word_mat", initializer=tf.constant(
                word_mat, dtype=tf.float32), trainable=False)
            self.char_mat = tf.get_variable(
                "char_mat", initializer=tf.constant(char_mat, dtype=tf.float32))

            # To compute mask, cast context and question to bool
            # context is of shape (batch_size, max_sentence_num, max_words_per_sentence)
            # we need to cast it to bool and reduce_sum two times along the word axis and the sentence axis
            # sentence mask
            # context mask
            # question mask

            self.s_mask, self.s_len = self.compute_mask_and_len(self.c)  # [N, M, J], [N, M]
            self.c_mask, self.c_len = self.compute_mask_and_len(self.s_len)  # [N]

            self.q_mask, self.q_len = self.compute_mask_and_len(self.q)

            self.a_mask, self.a_len = self.compute_mask_and_len(self.a)  # [N, M, J], [N, M], candidate
            self.d_mask, self.d_len = self.compute_mask_and_len(self.a_len)  # [N]

            N, CL = config.batch_size if not self.demo else 1, config.char_limit
            self.c_maxlen = tf.reduce_max(self.c_len)
            self.s_maxlen = tf.reduce_max(self.s_len)
            self.q_maxlen = tf.reduce_max(self.q_len)
            self.d_maxlen = tf.reduce_max(self.d_len)
            self.a_maxlen = tf.reduce_max(self.a_len)

            self.c = tf.slice(self.c, [0, 0, 0], [N, self.c_maxlen, self.s_maxlen])
            self.s_len = tf.slice(self.s_len, [0, 0], [N, self.c_maxlen])
            self.q = tf.slice(self.q, [0, 0], [N, self.q_maxlen])
            self.a = tf.slice(self.a, [0, 0, 0], [N, self.d_maxlen, self.a_maxlen])
            self.a_len = tf.slice(self.a_len, [0, 0], [N, self.d_maxlen])

            self.s_mask = tf.slice(self.s_mask, [0, 0, 0], [N, self.c_maxlen, self.s_maxlen])
            self.c_mask = tf.slice(self.c_mask, [0, 0], [N, self.c_maxlen])
            self.q_mask = tf.slice(self.q_mask, [0, 0], [N, self.q_maxlen])
            self.a_mask = tf.slice(self.a_mask, [0, 0, 0], [N, self.d_maxlen, self.a_maxlen])
            self.d_mask = tf.slice(self.d_mask, [0, 0], [N, self.d_maxlen])

            self.ch = tf.slice(self.ch, [0, 0, 0, 0], [N, self.c_maxlen, self.s_maxlen, CL])
            self.qh = tf.slice(self.qh, [0, 0, 0], [N, self.q_maxlen, CL])
            self.ah = tf.slice(self.ah, [0, 0, 0, 0], [N, self.d_maxlen, self.a_maxlen, CL])
            self.su = tf.slice(self.su, [0, 0], [N, self.c_maxlen])

            # self.ch_len = tf.reshape(tf.reduce_sum(
            #     tf.cast(tf.cast(self.ch, tf.bool), tf.int32), axis=2), [-1])
            # self.qh_len = tf.reshape(tf.reduce_sum(
            #     tf.cast(tf.cast(self.qh, tf.bool), tf.int32), axis=2), [-1])
            # self.ah_len = tf.reshape(tf.reduce_sum(
            #     tf.cast(tf.cast(self.ah, tf.bool), tf.int32), axis=2), [-1])

            self.forward()
            total_params()

            if trainable:
                self.lr = tf.minimum(config.learning_rate,
                                     0.001 / tf.log(999.) * tf.log(tf.cast(self.global_step, tf.float32) + 1))
                self.opt = tf.train.AdamOptimizer(learning_rate=self.lr, beta1=0.8, beta2=0.999, epsilon=1e-7)
                # self.lr = tf.get_variable(
                #     "lr", shape=[], dtype=tf.float32, trainable=False)
                # self.opt = tf.train.AdadeltaOptimizer(learning_rate=1, epsilon=1e-6)
                grads = self.opt.compute_gradients(self.loss)
                gradients, variables = zip(*grads)
                capped_grads, _ = tf.clip_by_global_norm(
                    gradients, config.grad_clip)
                self.train_op = self.opt.apply_gradients(
                    zip(capped_grads, variables), global_step=self.global_step)

    @staticmethod
    def compute_mask_and_len(tensor):
        tensor_mask = tf.cast(tensor, tf.bool)
        tensor_len = tf.reduce_sum(tf.cast(tensor_mask, tf.int32), axis=-1)
        return tensor_mask, tensor_len

    def forward(self):
        config = self.config
        N, M, D, JX, JQ, JA, CL, d, dc, nh = (config.batch_size if not self.demo else 1,
                                              self.c_maxlen,
                                              self.d_maxlen,
                                              self.s_maxlen,
                                              self.q_maxlen,
                                              self.a_maxlen,
                                              config.char_limit,
                                              config.hidden,
                                              config.char_dim,
                                              config.num_heads)
        # gru = CuDnnGRU if config.use_cudnn else NativeGRU
        gru = NativeGRU

        with tf.variable_scope("Input_Embedding_Layer"):
            # with tf.variable_scope("char"):
            #     ch_emb = tf.reshape(tf.nn.embedding_lookup(
            #         self.char_mat, self.ch), [JX * N * M, CL, dc])
            #     qh_emb = tf.reshape(tf.nn.embedding_lookup(
            #         self.char_mat, self.qh), [JQ * N, CL, dc])
            #     ah_emb = tf.reshape(tf.nn.embedding_lookup(
            #         self.char_mat, self.ah), [JA * N * D, CL, dc])
            #     ch_emb = tf.nn.dropout(ch_emb, 1.0 - 0.5 * self.dropout)
            #     qh_emb = tf.nn.dropout(qh_emb, 1.0 - 0.5 * self.dropout)
            #     ah_emb = tf.nn.dropout(ah_emb, 1.0 - 0.5 * self.dropout)
            #
            #     # Bidaf style conv-highway encoder
            #     ch_emb = conv(ch_emb, d,
            #                   bias=True, activation=tf.nn.relu, kernel_size=5, name="char_conv", reuse=None)
            #     qh_emb = conv(qh_emb, d,
            #                   bias=True, activation=tf.nn.relu, kernel_size=5, name="char_conv", reuse=True)
            #     ah_emb = conv(ah_emb, d,
            #                   bias=True, activation=tf.nn.relu, kernel_size=5, name="char_conv", reuse=True)
            #
            #     ch_emb = tf.reduce_max(ch_emb, axis=1)
            #     qh_emb = tf.reduce_max(qh_emb, axis=1)
            #     ah_emb = tf.reduce_max(ah_emb, axis=1)
            #
            #     ch_emb = tf.reshape(ch_emb, [-1, M, JX, d])
            #     qh_emb = tf.reshape(qh_emb, [-1, JQ, d])
            #     ah_emb = tf.reshape(ah_emb, [-1, D, JA, d])

            with tf.variable_scope("word"):
                # [N, M, JX, d]
                c_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.word_mat, self.c), 1.0 - self.dropout)
                # [N, JQ, d]
                q_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.word_mat, self.q), 1.0 - self.dropout)
                # [N, D, JA, d]
                a_emb = tf.nn.dropout(tf.nn.embedding_lookup(self.word_mat, self.a), 1.0 - self.dropout)
            #
            # c_emb = tf.concat([c_emb, ch_emb], axis=-1)
            # q_emb = tf.concat([q_emb, qh_emb], axis=-1)
            # a_emb = tf.concat([a_emb, ah_emb], axis=-1)

            # highway network
            # with tf.variable_scope("highway"):
            #     c_emb = highway_network(c_emb, config.highway_num_layers, True, wd=config.wd, is_train=self.is_train)
            #     tf.get_variable_scope().reuse_variables()
            #     q_emb = highway_network(q_emb, config.highway_num_layers, True, wd=config.wd, is_train=self.is_train)
            # q_emb = tf.expand_dims(tf.ones([tf.shape(q_emb)[0], 1]), 1) * q_emb

            # q_emb = tf.tile(tf.expand_dims(q_emb, 1), [1, tf.shape(a_emb)[1], 1, 1])
            # q_emb = tf.concat([a_emb, q_emb], axis=2)  # [N, D, JA + JQ, 2d]
            #
            # q_mask = tf.tile(tf.expand_dims(self.q_mask, 1), [1, tf.shape(self.q_mask)[1], 1])
            # qa_mask = tf.concat([tf.cast(tf.ones_like(self.a_mask), tf.bool), q_mask], axis=2)  # [N, D, JA + JQ, 2d]
            # qa_len = tf.reduce_sum(tf.cast(qa_mask, tf.int32), axis=-1)

        with tf.variable_scope("Embedding_Encoder_Layer"):
            d_cell_fw = SwitchableDropoutWrapper(BasicLSTMCell(d, state_is_tuple=True),
                                                 self.is_train, input_keep_prob=config.input_keep_prob)
            d_cell_bw = SwitchableDropoutWrapper(BasicLSTMCell(d, state_is_tuple=True),
                                                 self.is_train, input_keep_prob=config.input_keep_prob)

            # question encoding
            (fw_u, bw_u), ((_, fw_u_f), (_, bw_u_f)) = bidirectional_dynamic_rnn(d_cell_fw, d_cell_bw,
                                                                                 q_emb,
                                                                                 self.q_len,
                                                                                 dtype='float',
                                                                                 scope='u1')  # [N, J, d], [N, d]
            q_encoded = tf.concat(axis=-1, values=[fw_u, bw_u])  # [N, D, JA + JQ, 2d]

            cell_fw = SwitchableDropoutWrapper(BasicLSTMCell(d, state_is_tuple=True),
                                               self.is_train, input_keep_prob=config.input_keep_prob)
            cell_bw = SwitchableDropoutWrapper(BasicLSTMCell(d, state_is_tuple=True),
                                               self.is_train, input_keep_prob=config.input_keep_prob)
            (fw_h, bw_h), _ = bidirectional_dynamic_rnn(cell_fw, cell_bw,
                                                        c_emb,
                                                        self.s_len,
                                                        dtype='float',
                                                        scope='h1')  # [N, M, JX, 2d]
            c_encoded = tf.concat(axis=-1, values=[fw_h, bw_h])  # [N, M, JX, 2d]

            (fw_h, bw_h), _ = bidirectional_dynamic_rnn(cell_fw, cell_bw,
                                                        a_emb,
                                                        self.a_len,
                                                        dtype='float',
                                                        scope='h1')  # [N, M, JX, 2d]
            a_encoded = tf.concat(axis=-1, values=[fw_h, bw_h])  # [N, D, JA, 2d]

        with tf.variable_scope("attention"):
            c_encoded = tf.reshape(c_encoded, shape=(-1, M * JX, 2 * d))
            qc_att = dot_attention(c_encoded, q_encoded, mask=self.q_mask, hidden=d,
                                   keep_prob=config.input_keep_prob, is_train=self.is_train)
            rnn = gru(num_layers=1, num_units=d, batch_size=N,
                      input_size=qc_att.get_shape().as_list()[-1],
                      keep_prob=config.input_keep_prob,
                      is_train=self.is_train)
            mat = rnn(qc_att, seq_len=self.c_len)

        mask = tf.reshape(self.s_mask, shape=(-1, M * JX))

        with tf.variable_scope("match"):
            self_att = dot_attention(mat, mat, mask=mask, hidden=d,
                                     keep_prob=config.input_keep_prob,
                                     is_train=self.is_train)
            rnn = gru(num_layers=1, num_units=d, batch_size=N,
                      input_size=self_att.get_shape().as_list()[-1],
                      keep_prob=config.input_keep_prob, is_train=self.is_train)
            match = rnn(self_att, seq_len=self.c_len)

        with tf.variable_scope("answer"):
            a_encoded = tf.reshape(a_encoded, shape=(-1, D * JA, 2 * d))
            a_att = dot_attention(a_encoded, match, mask=mask, hidden=d,
                                  keep_prob=config.input_keep_prob,
                                  is_train=self.is_train)
            rnn = gru(num_layers=1, num_units=d, batch_size=N,
                      input_size=a_att.get_shape().as_list()[-1],
                      keep_prob=config.input_keep_prob,
                      is_train=self.is_train)
            mat = rnn(a_att, seq_len=self.d_len)

        with tf.variable_scope("output"):
            mat = tf.reshape(mat, shape=(N, D, JA, 2 * d))  # [N, D, JA, 2d]
            s = tf.reduce_max(mat, axis=2)  # [N, D, 2d]

            W = tf.get_variable("W", [2 * d, 2])
            b = tf.get_variable("b", [2], initializer=tf.constant_initializer(0.))
            s = tf.tensordot(s, W, axes=[[2], [0]])
            s = tf.nn.bias_add(s, b)
            s = tf.nn.softmax(s)

            logit = s[:, :, 1]

            self.debug = s
            self.label_logits = tf.cast(tf.argmax(s, axis=-1), tf.float32)
            y = tf.slice(self.y, [0, 0], [N, D])
            self.loss = tf.reduce_mean(
                tf.losses.softmax_cross_entropy(onehot_labels=tf.stop_gradient(tf.cast(y, tf.float32)),
                                                logits=logit)) \
                        + tf.reduce_mean(
                tf.losses.sigmoid_cross_entropy(multi_class_labels=tf.stop_gradient(tf.cast(y, tf.float32)),
                                                logits=self.label_logits))

        # with tf.variable_scope("answer"):
        #     # att = tf.reshape(match, shape=(-1, self.c_maxlen, self.s_maxlen, 4 * d))  # [N, M, JX, 4d]
        #     att = tf.reduce_max(match, axis=1)
        #     self.label_logits = tf.nn.softmax(dense(att, 2, use_bias=False, scope="label"))
        #     self.losses1_test = tf.nn.weighted_cross_entropy_with_logits(
        #         logits=self.label_logits,
        #         targets=tf.stop_gradient(tf.cast(self.y, tf.float32)),
        #         pos_weight=tf.constant([0.5, 1.0]))
        #     self.losses1 = tf.reduce_mean(self.losses1_test)
        #
        # with tf.variable_scope("output"):
        #     qc_att = tf.reshape(qc_att, shape=(-1, self.c_maxlen, self.s_maxlen, 4 * d))  # [N, M, JX, 4d]
        #     # [N, M, 4d]
        #     match = tf.reduce_max(qc_att, axis=2)
        #     # [N, M, d]
        #     s = dense(match, 1, use_bias=False, scope="s0")
        #
        #     # [N, M, 1]
        #     # s = dense(s0, 1, use_bias=False, scope="s")
        #     # s = tf.squeeze(s, [2])
        #
        #     # [N, M]
        #     s1 = softmax_mask(tf.squeeze(s, [2]), self.c_mask)
        #     self.logits = tf.nn.sigmoid(s1)
        #
        #     # [N, M]
        #     # s1 = softmax_mask(tf.squeeze(s, [2]), self.c_mask)
        #     # self.logits = tf.nn.softmax(s1)
        #
        #     self.losses2 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
        #         logits=s1, labels=tf.stop_gradient(tf.cast(self.su, tf.float32))))
        #     self.loss = self.losses1 + self.losses2
        # self.loss = self.losses1
        #
        # if config.decay is not None:
        #     self.var_ema = tf.train.ExponentialMovingAverage(config.decay)
        #     ema_op = self.var_ema.apply(tf.trainable_variables())
        #     with tf.control_dependencies([ema_op]):
        #         self.loss = tf.identity(self.loss)
        #
        #         self.assign_vars = []
        #         for var in tf.global_variables():
        #             v = self.var_ema.average(var)
        #             if v:
        #                 self.assign_vars.append(tf.assign(var, v))

    def get_loss(self):
        return self.loss

    def get_global_step(self):
        return self.global_step
