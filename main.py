import os
import ujson as json
from json import JSONEncoder
from pprint import pprint

import numpy as np
import tensorflow as tf
from sklearn.metrics import precision_recall_fscore_support
from tqdm import tqdm

'''
This file is taken and modified from R-Net by HKUST-KnowComp
https://github.com/HKUST-KnowComp/R-Net
'''

from model import Model
from demo import Demo
from util import get_record_parser, get_batch_dataset, get_dataset


def debug(config):
    with open(config.word_emb_file, "r") as fh:
        word_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.char_emb_file, "r") as fh:
        char_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.train_eval_file, "r") as fh:
        train_eval_file = json.load(fh)
    with open(config.dev_eval_file, "r") as fh:
        dev_eval_file = json.load(fh)
    with open(config.dev_meta, "r") as fh:
        meta = json.load(fh)

    dev_total = meta["total"]
    print("Building model...")
    parser = get_record_parser(config)
    graph = tf.Graph()
    with graph.as_default() as g:
        train_dataset = get_batch_dataset(config.train_record_file, parser, config)
        dev_dataset = get_dataset(config.dev_record_file, parser, config)
        handle = tf.placeholder(tf.string, shape=[])
        iterator = tf.data.Iterator.from_string_handle(
            handle, train_dataset.output_types, train_dataset.output_shapes)
        train_iterator = train_dataset.make_one_shot_iterator()
        dev_iterator = dev_dataset.make_one_shot_iterator()

        model = Model(config, iterator, word_mat, char_mat, graph=g)

        sess_config = tf.ConfigProto(allow_soft_placement=True)
        sess_config.gpu_options.allow_growth = True

        loss_save = 100.0
        patience = 0
        best_f1 = 0.
        best_em = 0.

        with tf.Session(config=sess_config) as sess:
            writer = tf.summary.FileWriter(config.log_dir)
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            train_handle = sess.run(train_iterator.string_handle())
            dev_handle = sess.run(dev_iterator.string_handle())
            if os.path.exists(os.path.join(config.save_dir, "checkpoint")):
                saver.restore(sess, tf.train.latest_checkpoint(config.save_dir))
            global_step = max(sess.run(model.global_step), 1)

            for _ in tqdm(range(global_step, config.num_steps + 1)):
                global_step = sess.run(model.global_step) + 1
                debug = sess.run([model.debug],
                                 feed_dict={
                                     handle: train_handle, model.dropout: config.dropout})
                print(debug)
                print(debug[0].shape)


def train(config):
    with open(config.word_emb_file, "r") as fh:
        word_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.char_emb_file, "r") as fh:
        char_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.train_eval_file, "r") as fh:
        train_eval_file = json.load(fh)
    with open(config.dev_eval_file, "r") as fh:
        dev_eval_file = json.load(fh)
    with open(config.dev_meta, "r") as fh:
        meta = json.load(fh)

    dev_total = meta["total"]
    print("Building model...")
    parser = get_record_parser(config)
    graph = tf.Graph()
    with graph.as_default() as g:
        train_dataset = get_batch_dataset(config.train_record_file, parser, config)
        dev_dataset = get_dataset(config.dev_record_file, parser, config)
        handle = tf.placeholder(tf.string, shape=[])
        iterator = tf.data.Iterator.from_string_handle(
            handle, train_dataset.output_types, train_dataset.output_shapes)
        train_iterator = train_dataset.make_one_shot_iterator()
        dev_iterator = dev_dataset.make_one_shot_iterator()

        model = Model(config, iterator, word_mat, char_mat, graph=g)

        sess_config = tf.ConfigProto(allow_soft_placement=True)
        sess_config.gpu_options.allow_growth = True

        loss_save = 100.0
        patience = 0
        best_f1 = 0.
        best_em = 0.

        with tf.Session(config=sess_config) as sess:
            writer = tf.summary.FileWriter(config.log_dir)
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()
            train_handle = sess.run(train_iterator.string_handle())
            dev_handle = sess.run(dev_iterator.string_handle())
            if os.path.exists(os.path.join(config.save_dir, "checkpoint")):
                saver.restore(sess, tf.train.latest_checkpoint(config.save_dir))
            global_step = max(sess.run(model.global_step), 1)

            for _ in tqdm(range(global_step, config.num_steps + 1)):
                global_step = sess.run(model.global_step) + 1
                loss, train_op = sess.run([model.loss,
                                           model.train_op],
                                          feed_dict={
                                              handle: train_handle, model.dropout: config.dropout})
                if global_step % config.period == 0:
                    loss_sum = tf.Summary(value=[tf.Summary.Value(
                        tag="model/loss", simple_value=loss), ])
                    writer.add_summary(loss_sum, global_step)
                    # loss1_sum = tf.Summary(value=[tf.Summary.Value(
                    #     tag="model/loss_label", simple_value=loss1), ])
                    # writer.add_summary(loss1_sum, global_step)
                    # loss2_sum = tf.Summary(value=[tf.Summary.Value(
                    #     tag="model/loss_sentence", simple_value=loss2), ])
                    # writer.add_summary(loss2_sum, global_step)
                if global_step % config.checkpoint == 0:
                    _, summ = evaluate_batch(
                        model, config.val_num_batches, train_eval_file, sess, "train", handle, train_handle)
                    for s in summ:
                        writer.add_summary(s, global_step)

                    metrics, summ = evaluate_batch(
                        model, dev_total // config.batch_size + 1, dev_eval_file, sess, "dev", handle, dev_handle)

                    # dev_f1 = metrics["f1"]
                    # dev_em = metrics["exact_match"]
                    # if dev_f1 < best_f1 and dev_em < best_em:
                    #     patience += 1
                    #     if patience > config.early_stop:
                    #         break
                    # else:
                    #     patience = 0
                    #     best_em = max(best_em, dev_em)
                    #     best_f1 = max(best_f1, dev_f1)

                    for s in summ:
                        writer.add_summary(s, global_step)
                    writer.flush()
                    filename = os.path.join(
                        config.save_dir, "model_{}.ckpt".format(global_step))
                    saver.save(sess, filename)


def evaluate_batch(model, num_batches, eval_file, sess, data_type, handle, str_handle):
    losses = []
    P1 = []
    R1 = []
    for _ in tqdm(range(1, num_batches + 1)):
        qa_id, loss, label_logits = sess.run(
            [model.qa_id, model.loss, model.label_logits],
            feed_dict={handle: str_handle})
        for qid, label_logit in zip(qa_id, label_logits):
            sentences_used = eval_file[str(qid)]["sentences_used"]
            sentences = eval_file[str(qid)]["context"]
            question = eval_file[str(qid)]["question"]
            answer = eval_file[str(qid)]["answer"]
            y = eval_file[str(qid)]["y"]
            # top5 = logit.argsort()[-5:]
            # threshold = set(np.argwhere(logit >= 0.5).reshape(-1))
            # top5 = set(top5).intersection(threshold)
            # if set(sentences_used).issubset(set(top5)):
            #     in_top5.append(1)
            # else:
            #     in_top5.append(len(set(sentences_used).intersection(set(top5))) / len(sentences_used))
            if data_type == 'dev':
                step = 100
            else:
                step = 100
            if qid % step == 0:
                print('===================[{}]=========================='.format(data_type))
                print('Question {}: {}'.format(qid, question))
                for i, ans in enumerate(answer):
                    if i > label_logit.shape[0]:
                        predict = 0
                    else:
                        predict = label_logit[i]
                    print('Answer: {} - {} - {}'.format(ans['text'], ans['isAnswer'], predict))
                print('------------Used sentences: {}'.format(sentences_used))
                for i in sentences_used:
                    print(sentences[i])
            predictedAns = np.array(label_logit > 0.5, np.int)
            for i in range(len(y)):
                if i >= predictedAns.shape[0]:
                    predictedAns[i] = 0
            correctAns = [int(a) for a in y]
            predictCount = sum(predictedAns)
            correctCount = sum(correctAns)
            agreementCount = sum([a * b for (a, b) in zip(correctAns, predictedAns)])
            p1 = (1.0 * agreementCount / correctCount) if correctCount > 0.0 else 1.0
            r1 = (1.0 * agreementCount / predictCount) if predictCount > 0.0 else 1.0
            P1.append(p1)
            R1.append(r1)
        losses.append(loss)
    loss = np.mean(losses)

    print("Per question measures (i.e. precision-recall per question, then average) ")

    precision = np.mean(P1)
    recall = np.mean(R1)
    f_score = 2 * recall * precision / (precision + recall)
    print("\tP: {} - R: {} - F1m: {}".format(precision, recall, f_score))
    metrics = {"loss": loss, "precision": precision, "recall": recall, "f_score": f_score}
    loss_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/loss".format(data_type), simple_value=metrics["loss"]), ])
    precision_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/precision".format(data_type), simple_value=metrics["precision"]), ])
    recall_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/recall".format(data_type), simple_value=metrics["recall"]), ])
    f_score_sum = tf.Summary(value=[tf.Summary.Value(
        tag="{}/f_score".format(data_type), simple_value=metrics["f_score"]), ])
    # f1_sum = tf.Summary(value=[tf.Summary.Value(
    #     tag="{}/f1".format(data_type), simple_value=metrics["f1"]), ])
    # em_sum = tf.Summary(value=[tf.Summary.Value(
    #     tag="{}/em".format(data_type), simple_value=metrics["exact_match"]), ])
    return metrics, [loss_sum, precision_sum, recall_sum, f_score_sum]


def demo(config):
    with open(config.word_emb_file, "r") as fh:
        word_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.char_emb_file, "r") as fh:
        char_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.test_meta, "r") as fh:
        meta = json.load(fh)

    model = Model(config, None, word_mat, char_mat, trainable=False, demo=True)
    demo = Demo(model, config)


def test(config):
    with open(config.word_emb_file, "r") as fh:
        word_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.char_emb_file, "r") as fh:
        char_mat = np.array(json.load(fh), dtype=np.float32)
    with open(config.test_eval_file, "r") as fh:
        eval_file = json.load(fh)
    with open(config.test_meta, "r") as fh:
        meta = json.load(fh)

    total = meta["total"]

    graph = tf.Graph()
    print("Loading model...")
    with graph.as_default() as g:
        test_batch = get_dataset(config.test_record_file, get_record_parser(
            config, is_test=True), config).make_one_shot_iterator()

        model = Model(config, test_batch, word_mat, char_mat, trainable=False, graph=g)

        sess_config = tf.ConfigProto(allow_soft_placement=True)
        sess_config.gpu_options.allow_growth = True

        with tf.Session(config=sess_config) as sess:
            sess.run(tf.global_variables_initializer())
            saver = tf.train.Saver()

            if config.load_step > 0:
                save_path = os.path.join(config.save_dir, "model_{}.ckpt".format(config.load_step))
            else:
                save_dir = config.save_dir
                checkpoint = tf.train.get_checkpoint_state(save_dir)
                assert checkpoint is not None, "cannot load checkpoint at {}".format(save_dir)
                save_path = checkpoint.model_checkpoint_path
                # tf.train.latest_checkpoint(config.save_dir)

            saver.restore(sess, save_path)
            if config.decay < 1.0:
                sess.run(model.assign_vars)
            losses = []
            data = {}
            result = []
            for step in tqdm(range(total // config.batch_size + 1)):
                qa_id, loss, label_logits = sess.run(
                    [model.qa_id, model.loss, model.label_logits])
                losses.append(loss)
                for qid, label_logit in zip(qa_id, label_logits):
                    q = eval_file[str(qid)]
                    data.setdefault(q['pid'], {})
                    data[q['pid']].setdefault(q['qid'], [0] * int(q['answers_len']))
                    data[q['pid']][q['qid']][q['aid']] = int(label_logit.argmax())

            for p in data:
                for q in data[p]:
                    result.append({
                        'pid': p,
                        'qid': str(q),
                        'scores': data[p][q]
                    })
            print(result)
            loss = np.mean(losses)
            with open('data/predict.json', 'w') as f:
                json.dump(result, f, )


class CustomJSONEncoder(JSONEncoder):
    '''
    Custom JSON encoder that converts numpy.float64 values to strings.
    '''

    def default(self, val):
        if isinstance(val, np.int32):
            return int(val)
        if isinstance(val, np.int64):
            return int(val)
        if isinstance(val, np.number):
            return str(val)
        if isinstance(val, np.ndarray):
            lst = []
            for v in val:
                lst.append(str(v))
            return lst
        return JSONEncoder.default(self, val)
