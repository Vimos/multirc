import json
import os
import re

from allennlp.predictors import Predictor
from nltk import word_tokenize
from stanfordcorenlp import StanfordCoreNLP

nlp = StanfordCoreNLP(path_or_host='http://192.168.0.101', port=9000)
text_re = re.compile('<b>Sent \d+: </b>(.*?)<br>')

data_dir = os.path.join(os.getcwd(), 'datasets')
json_dir = os.path.join(data_dir, 'mutlirc-v2', 'splitv2')

dev_json = os.path.join(json_dir, 'dev_83-fixedIds.json')
train_json = os.path.join(json_dir, 'train_456-fixedIds.json')


def iter_doc():
    with open(train_json, "r") as fh:
        source = json.load(fh)
        r = []
        q = []
        s = []
        for para in source['data']:
            sentences = text_re.findall(para['paragraph']["text"].replace(
                "''", '" ').replace("``", '" '))
            # context_tokens = []
            # context_chars = []
            # for s1, s2 in zip(sentences, sentences[1:]):
            #     print('--------------------------------------------')
            #     print(s1, s2)
            #     d = json.loads(nlp.annotate(s1 + s2, properties={"annotators": "ner,coref"}))
            #     # pprint(d['sentences'][0]['entitymentions'])
            #     pprint(d['corefs'])
            # break
            pid = para['id'].replace('/', '_')
            context_tokens = []
            context_chars = []
            for sentence in sentences:
                sentence_tokens = word_tokenize(sentence)
                s.append(len(sentence_tokens))
            # spans = convert_idx(sentence, context_tokens)
            # context_tokens.append(sentence_tokens)
            # context_chars.append([list(token) for token in sentence_tokens])
            # with open('data/coref/{}_.jsonl'.format(pid), 'w') as f:
            d = {"document": ' '.join(sentences)}
            yield d
            # json.dump(d, f)
            for idx, qa in enumerate(para['paragraph']['questions']):
                ques = qa["question"].replace(
                    "''", '" ').replace("``", '" ')
                sentences_used = qa['sentences_used']
                # ques_tokens = word_tokenize(ques)
                # ques_chars = [list(token) for token in ques_tokens]
                r.append(len(qa['answers']))
                q.append(len(qa['question']))

                #     p = []
                for i, ans in enumerate(qa['answers']):
                    # with open('data/coref/{}_{}_{}.jsonl'.format(pid, idx, i), 'w') as f:
                    d = {"document": ques + ' ' + ans['text']}
                    yield d
                    # json.dump(d, f)
            #         # an = ans["text"].replace(
            #         #     "''", '" ').replace("``", '" ')
            #         # d = json.loads(nlp.annotate(ques + ' ' + an, properties={"annotators": "ner,coref"}))
            #         # if d['corefs']:
            #         #     print('======================================================')
            #         #     print("Question: {}".format(ques))
            #         #     print("Answer  : {}".format(an))
            #         #     print('------------Used sentences: {}'.format(sentences_used))
            #         #     for i in sentences_used:
            #         #         print(sentences[i])
            #         #     pprint(d['corefs'])
            #         p.append(ans['isAnswer'])
            #     print(sum(p) / len(p))
            #     a += sum(p)
            #     t += len(p)
            # print(a / t)
        # print(max(r))
        # print(q)
    # print(sorted(q)[-5:])


predictor = Predictor.from_path("/home/vimos/git/Coref/coref-model-2018.02.05.tar.gz")
for d in iter_doc():
    results = predictor.predict_json(d)
    # for word, tag in zip(results["words"], results["tags"]):
    #     print(f"{word}\t{tag}")
    print(d)
    print(results)
    for cluster in results['clusters']:
        for w in cluster:
            print(' '.join(results['document'][w[0]:w[1] + 1]), end=', ')
    print()
